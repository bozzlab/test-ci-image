FROM python:3.6-slim
ENV TZ=Asia/Bangkok
WORKDIR /app
COPY . /app
RUN apt-get update \
    && pip install --upgrade pip \
    && pip install --ignore-installed pyOpenSSL --upgrade \
    && pip install --no-cache-dir -r requirements.txt \ 
    && rm -rf /var/lib/apt/lists/*
CMD python main.py